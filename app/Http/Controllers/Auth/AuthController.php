<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\User;
use Validator;
use Auth;
use App\Http\Controllers\Controller;

class AuthController extends Controller
{

    public function getSignin()
    {
        return view('auth.signin');
    }


    public function postSignin(Request $request)
    {
        $this->validate($request, [
            'email' => 'required',
            'password' => 'required'
        ]);

        if(!Auth::attempt($request->only('email', 'password'), $request->has('remember')))
        {
            return redirect()->back()->with('info', 'Could not sign you in with those details.');
        }

        return redirect()->route('admin.links.index')->with('info', 'Sing in success');

    }

    public function getSignout()
    {
        Auth::logout();
        return redirect('/');
    }
}

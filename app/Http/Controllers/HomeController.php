<?php

namespace App\Http\Controllers;

use App\Models\Language;
use Validator;
use App\Models\Country;
use App\Models\UserInterface;
use App\Models\Category;
use App\Models\Settings;
use App\Models\Links;
use App\Models\ClickStat;
use App\Models\LikeStat;
use App\Models\CategoryTranslate;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Mail;

use Session;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, $lang_user)
    {

        Session::put('lang', $lang_user);
        
        $lang = Language::all();
        
        $id_lang = Language::where('lang_code', '=', $lang_user)->get();


        $id_lang = $id_lang->all();

        $input = $request->all();

        $country = Country::all();


        $interface = '';

        if(isset($id_lang[0])){
            $UserInterface = UserInterface::findOrNew($id_lang[0]->id);
            $interface = json_decode($UserInterface->json_translate);
        } else {
            $UserInterface = UserInterface::all();
        }

        $countrys = array();

        foreach($country->all() as $key => $val){
            $countrys[$val->id] = $val->country;
        }

        if(isset($input['category']) && count($input['category']) > 0 && $input['category'][0] > 0){
            $category_show = Category::whereIn('id', $input['category'] )->get();
        } else {
            $category_show = Category::all();
        }

        if(!isset($input['sortable'])){
            $input['sortable'] = 'count_click';
        }
        
        if(isset($input['page'])){
            $end = $input['page'] * 10;
            $start = $end - 10;
        } else {
            $end = 10;
            $start = $end - 10;
        }

        $count = 0;

        $links = array();

        foreach($category_show->all() as $key => $val){
            if(!count($id_lang) > 0){
                $links[$val->id]['category'] = $val->category;
            } else {
                $categoryTrans = CategoryTranslate::where('language_id', '=', $id_lang[0]->id)->where('category_id', '=', $val->id)->get();
                $categoryTrans = $categoryTrans->all();
                if(count($categoryTrans) > 0){
                    $links[$val->id]['category'] = $categoryTrans[0]->translate;
                } else {
                    $links[$val->id]['category'] = $val->category;
                }
            }
            if(count($input) > 0 && isset($input['country']) && $input['country'] > 0){
                $country = Country::findOrFail($input['country']);
                $categoryTrans = CategoryTranslate::where('language_id', '=', $country->language_id)->where('category_id', '=', $val->id)->get();
                $categoryTrans = $categoryTrans->all();

                if(count($id_lang) > 0 && $country->language_id != $id_lang[0]->id ){
                    if(count($categoryTrans) > 0 ){
                        $links[$val->id]['category_trans'] = $categoryTrans[0]->translate;
                    }
                }

                $link_category = Links::select('title', 'likes', 'icon', 'id')
                    ->where('category_id', '=', $val->id)
                    ->where('country_id', '=', $input['country'])
                    ->where('published', '=', 1)
                    ->orderBy($input['sortable'], 'desc')->skip($start)->take($end)->get();
                $link_categor_count = Links::where('category_id', '=', $val->id)
                    ->where('country_id', '=', $input['country'])
                    ->where('published', '=', 1)
                    ->orderBy($input['sortable'], 'desc')->count();
            } else {
                $link_category = Links::select('title', 'likes', 'icon', 'id')
                    ->where('category_id', '=', $val->id)
                    ->where('published', '=', 1)->orderBy($input['sortable'], 'desc')->skip($start)->take($end)->get();
                $link_categor_count = Links::where('category_id', '=', $val->id)
                    ->where('published', '=', 1)->orderBy($input['sortable'], 'desc')->count();
            }

            $links[$val->id]['link_list'] = $link_category->all();
            
            if($link_categor_count > $count){
                $count = $link_categor_count;
            }
            
        }

        $categorys = array();

        $category = Category::all();

        foreach($category->all() as $key => $val){

            if(count($id_lang) > 0){
                $categoryTrans = CategoryTranslate::where('language_id', '=', $id_lang[0]->id)->where('category_id', '=', $val->id)->get();
                $categoryTrans = $categoryTrans->all();
                if(count($categoryTrans) > 0){
                    
                    $categorys[$val->id] = $categoryTrans[0]->translate;
                } else {
                    $categorys[$val->id] = $val->category;
                }
            } else {
                $categorys[$val->id] = $val->category;
            }

        }

        $setting = Settings::all();

        $settings = array();

        foreach($setting->all() as $key => $val){
            $settings[$val->name] = $val->values;
        }

    

        $sortable_arr = ['likes' => (isset($interface->like))?: 'Like' , 'count_click' => (isset($interface->count_click))?: 'Count Click'];

        return view('welcome')
            ->with('count', $count)
            ->with('sort_arr', $sortable_arr)
            ->with('interface', $interface)
            ->with('settings', $settings)
            ->with('lang', $lang)
            ->with('countrys', $countrys)
            ->with('categorys', $categorys)
            ->with('links', $links);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function click($id)
    {
        $links = Links::findOrFail($id);

        $ClickStat = new ClickStat();
        $ClickStat->link_id = $links->id;
        $ClickStat->save();

        $links->count_click++;

        $links->save();

        if (filter_var($links->link, FILTER_VALIDATE_URL) === FALSE) {
            return redirect()->away('http://'.$links->link);
        } else {
            return redirect()->away($links->link);
        }

    }

    public function like(Request $request)
    {

        if ($request->isMethod('post')){

            $this->validate($request, [
                'link_id' => 'required',
                'like' => 'required',
            ]);

            $id_lang = Language::where('lang_code', '=', Session::get('lang'))->get();

            if(isset($id_lang[0])){
                $UserInterface = UserInterface::findOrNew($id_lang[0]->id);
                $interface = json_decode($UserInterface->json_translate);
            }

            $input = $request->all();

            $linkFind = LikeStat::where('link_id', '=', $input['link_id'])->where('ip_user', '=', $this->getUserIP())->get();

            if($linkFind->count() > 0){
                return response()->json(['message' => isset($interface->like_is_write) ? $interface->like_is_write : 'You have paste like for this link!']);
                die();
            }

            $links = Links::findOrFail($input['link_id']);

            $ClickStat = new LikeStat();
            $ClickStat->link_id = $links->id;
            $ClickStat->ip_user = $this->getUserIP();
            $ClickStat->like = $input['like'];
            $ClickStat->save();

            if($input['like'] == 0){
                $links->likes--;
            } else {
                $links->likes++;
            }
            

            $links->save();

            return response()->json(['message' => isset($interface->like_is_write) ? $interface->like_is_write : 'Like is write']);
        }

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function AddLink(Request $request)
    {
        if ($request->isMethod('post')) {
            $input = $request->all();

            $validation = Validator::make($request->all(), [
                'country' => 'required|min:1',
                'category' => 'required|min:1',
                'link' => 'required|url',
                'captcha' => 'required|captcha',
            ]);

            if($validation->fails()){
                return response()->json(['status' => 0, 'message' => json_encode($validation->messages())]);
            }

            $input['published'] = 0;


            $hash_acti = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 50);

            $link = new Links;
            $link->country_id = $input['country'];
            $link->category_id = $input['category'];
            $link->link = $input['link'];
            $link->published = 0;
            if(isset($input['email'])){
                $link->active = 0;
                $link->hash_activation = $hash_acti;
                
                /*Mail::send('test', ['name' => 'Novica'], function($message){
                    $message->to('test-admin@mail.ru', 'Some Gus')->subject('dsadsadsa');
                });*/
                
            } else {
                $link->active = 1;
            }
            $link->save();

            /*Mail::send('test', ['name' => 'Novica'], function($message){
                $message->to('test-admin@mail.ru', 'Some Gus')->subject('dsadsadsa');
            });*/

            return response()->json(['status' => 1, 'message' => 'Link is add']);
        }
    }
    
    public function newCaptcha(Request $request)
    {
        
        if ($request->isMethod('post')) {
            
            $src_captcha = new \Mews\Captcha\Facades\Captcha();

            $src_captcha = captcha_src();
            
            return response()->json(['status' => 1, 'link' => $src_captcha]);
        }
        
    }
    
    public function activation_link($hash)
    {
        $links = Links::where('hash_activation', '=', $hash)->get();


        $links = $links->all();

        if(count($links) == 0){
            return 'Wrong hash';
        }
        
        
        
        $link = Links::findOrFail($links[0]->id);
        
        $link->active = 1;

        $link->save();

        return redirect('/');
        
        
    }
    
    public function getUserIP()
    {
        $client  = @$_SERVER['HTTP_CLIENT_IP'];
        $forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
        $remote  = $_SERVER['REMOTE_ADDR'];
    
        if(filter_var($client, FILTER_VALIDATE_IP))
        {
            $ip = $client;
        }
        elseif(filter_var($forward, FILTER_VALIDATE_IP))
        {
            $ip = $forward;
        }
        else
        {
            $ip = $remote;
        }
    
        return $ip;
    }
}
    
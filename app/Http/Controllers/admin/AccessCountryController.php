<?php

namespace App\Http\Controllers\Admin;

use App\Models\User;
use App\Models\AccessCountry;
use App\Models\Country;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class AccessCountryController extends Controller
{
    
    public function __construct(){
        
        $this->middleware('admin');
        
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $AccessCountry = AccessCountry::all();

        $AccessCountrys = array();

        foreach($AccessCountry->all() as $key => $val){
            $AccessCountrys[$val->id]['user'] = User::findOrNew($val->user_id)->full_name;
            $AccessCountrys[$val->id]['country'] = Country::findOrNew($val->country_id)->country;
        }

        return view('admin.accessCountry.index')
            ->with('AccessCountrys', $AccessCountrys);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $Country = Country::all();

        $Countrys = array();

        foreach($Country->all() as $key => $val){
            $Countrys[$val->id] = $val->country;
        }
        
        $User = User::all();

        $Users = array();

        foreach($User->all() as $key => $val){
            $Users[$val->id] = $val->full_name;
        }

        return view('admin.accessCountry.create')
            ->with('countrys', $Countrys)
            ->with('users', $Users);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();

        $this->validate($request, [
            'user_id' => 'required|min:1',
            'country_id' => 'required|min:1'
        ]);


        AccessCountry::create($input);

        return redirect()->route('admin.accessCountry.index')->with('info', 'AccessCountry is create!');
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $AccessCountry = AccessCountry::findOrFail($id);

        $Country = Country::all();

        $Countrys = array();

        foreach($Country->all() as $key => $val){
            $Countrys[$val->id] = $val->country;
        }
        
        $User = User::all();

        $Users = array();

        foreach($User->all() as $key => $val){
            $Users[$val->id] = $val->full_name;
        }

        return view('admin.accessCountry.edit')
            ->with('countrys', $Countrys)
            ->with('accessCountry', $AccessCountry)
            ->with('users', $Users);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $AccessCountry = AccessCountry::findOrFail($id);

        $this->validate($request, [
            'user_id' => 'required|min:1',
            'country_id' => 'required|min:1'
        ]);

        $input = $request->all();

        $AccessCountry->fill($input)->save();

        return redirect()->route('admin.accessCountry.index')->with('info', 'AccessCountry is update!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $AccessCountry = AccessCountry::findOrFail($id);

        $AccessCountry->delete();

        return redirect()->route('admin.accessCountry.index')->with('info', 'AccessCountry is delete!');
    }
}

<?php

namespace App\Http\Controllers\Admin;

use App\Models\Settings;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class SettingsController extends Controller
{
    
    public function __construct(){
        
        $this->middleware('admin');
        
    }
    
    
    public function index()
    {
        $settings = Settings::all();
        return view('admin.settings.index',['settings' => $settings]);
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $settings = Settings::findOrFail($id);

        return view('admin.settings.edit')->with('settings', $settings);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $settings = Settings::findOrFail($id);

        $this->validate($request, [
            'name' => 'required',
            'values' => 'required'
        ]);

        $input = $request->all();

        $settings->fill($input)->save();

        return redirect()->route('admin.settings.index')->with('info', 'Settings is update!');
    }
}

<?php

namespace App\Http\Controllers\Admin;

use App\Models\CategoryTranslate;
use App\Models\Language;
use App\Models\Category;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class CategoryTranslateController extends Controller
{
    
    public function __construct(){
        
        $this->middleware('admin');
        
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categoryTranslate = CategoryTranslate::all();

        $categoryTranslates = array();

        foreach($categoryTranslate->all() as $key => $val){
            $categoryTranslates[$val->id]['language'] = Language::findOrNew($val->language_id)->language;
            $categoryTranslates[$val->id]['category'] = Category::findOrNew($val->category_id)->category;
            $categoryTranslates[$val->id]['translate'] = $val->translate;
        }

        return view('admin.categoryTranslates.index')
            ->with('categoryTranslates', $categoryTranslates);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $language = Language::all();

        $languages = array();

        foreach($language->all() as $key => $val){
            $languages[$val->id] = $val->language;
        }

        $category = Category::all();

        $categorys = array();

        foreach($category->all() as $key => $val){
            $categorys[$val->id] = $val->category;
        }

        return view('admin.categoryTranslates.create')
            ->with('languages', $languages)
            ->with('categorys', $categorys);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();

        $this->validate($request, [
            'language_id' => 'required',
            'category_id' => 'required',
            'translate' => 'required'
        ]);


        CategoryTranslate::create($input);

        return redirect()->route('admin.category-translate.index')->with('info', 'Category Translates is create!');
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $categoryTranslate = CategoryTranslate::findOrFail($id);

        $language = Language::all();

        $languages = array();

        foreach($language->all() as $key => $val){
            $languages[$val->id] = $val->language;
        }

        $category = Category::all();

        $categorys = array();

        foreach($category->all() as $key => $val){
            $categorys[$val->id] = $val->category;
        }

        return view('admin.categoryTranslates.edit')
            ->with('languages', $languages)
            ->with('categorys', $categorys)
            ->with('categoryTranslate', $categoryTranslate);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $categoryTranslate = CategoryTranslate::findOrFail($id);

        $this->validate($request, [
            'language_id' => 'required',
            'category_id' => 'required',
            'translate' => 'required'
        ]);

        $input = $request->all();

        $categoryTranslate->fill($input)->save();

        return redirect()->route('admin.category-translate.index')->with('info', 'Category Translates is update!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = CategoryTranslate::findOrFail($id);

        $category->delete();

        return redirect()->route('admin.category-translate.index')->with('info', 'Category translate is delete!');
    }
}

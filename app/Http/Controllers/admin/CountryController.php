<?php

namespace App\Http\Controllers\Admin;

use App\Models\Language;
use App\Models\Country;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class CountryController extends Controller
{
    
    public function __construct(){
        
        $this->middleware('admin');
        
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $country = Country::all();

        $countries = array();

        foreach($country->all() as $key => $val){
            $countries[$val->id]['language'] = Language::findOrNew($val->language_id)->language;
            $countries[$val->id]['name'] = $val->country;
        }

        return view('admin.country.index')
            ->with('countries', $countries);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $language = Language::all();

        $languages = array();

        foreach($language->all() as $key => $val){
            $languages[$val->id] = $val->language;
        }

        return view('admin.country.create')
            ->with('languages', $languages);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();

        $this->validate($request, [
            'language_id' => 'required',
            'country' => 'required'
        ]);


        Country::create($input);

        return redirect()->route('admin.country.index')->with('info', 'Country is create!');
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $country = Country::findOrFail($id);

        $language = Language::all();

        $languages = array();

        foreach($language->all() as $key => $val){
            $languages[$val->id] = $val->language;
        }

        return view('admin.country.edit')
            ->with('languages', $languages)
            ->with('country', $country);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $country = Country::findOrFail($id);

        $this->validate($request, [
            'language_id' => 'required',
            'country' => 'required'
        ]);

        $input = $request->all();

        $country->fill($input)->save();

        return redirect()->route('admin.country.index')->with('info', 'Country is update!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $country = Country::findOrFail($id);

        $country->delete();

        return redirect()->route('admin.country.index')->with('info', 'Country is delete!');
    }
}

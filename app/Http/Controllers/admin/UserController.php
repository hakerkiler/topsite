<?php

namespace App\Http\Controllers\Admin;

use App\Models\User;
use App\Models\Language;
use App\Models\Country;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
    
    public function __construct(){
        
        $this->middleware('admin');
        
    }
    
    
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = User::all();


        return view('admin.user.index')
            ->with('users', $user);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $language = Language::all();

        $languages = array();

        foreach($language->all() as $key => $val){
            $languages[$val->id] = $val->language;
        }

        $country = Country::all();

        $countrys = array();

        foreach($country->all() as $key => $val){
            $countrys[$val->id] = $val->country;
        }

        return view('admin.user.create')
            ->with('languages', $languages)
            ->with('countrys', $countrys);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();

        $this->validate($request, [
            'language_id' => 'required|numeric|min:1',
            'country_id' => 'required|numeric|min:1',
            'full_name' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required',
            'role' => 'required',
        ]);

        $user = new User();
        $user->email = $input['email'];
        $user->country_id = $input['country_id'];
        $user->language_id = $input['language_id'];
        $user->role = $input['role'];
        $user->full_name = $input['full_name'];
        $user->password = bcrypt($input['password']);
        $user->save();

        return redirect()->route('admin.user.index')->with('info', 'User is create!');
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::findOrFail($id);

        $language = Language::all();

        $languages = array();

        foreach($language->all() as $key => $val){
            $languages[$val->id] = $val->language;
        }

        $country = Country::all();

        $countrys = array();

        foreach($country->all() as $key => $val){
            $countrys[$val->id] = $val->country;
        }

        return view('admin.user.edit')
            ->with('languages', $languages)
            ->with('countrys', $countrys)
            ->with('user', $user);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::findOrFail($id);

        $this->validate($request, [
            'language_id' => 'required|numeric|min:1',
            'country_id' => 'required|numeric|min:1',
            'full_name' => 'required',
            'email' => 'required|email',
            'role' => 'required',
        ]);

        $input = $request->all();

        $user->fill($input)->save();

        return redirect()->route('admin.user.index')->with('info', 'User  is update!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::findOrFail($id);

        $user->delete();

        return redirect()->route('admin.user.index')->with('info', 'User is delete!');
    }
}

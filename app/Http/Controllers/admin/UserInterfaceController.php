<?php

namespace App\Http\Controllers\Admin;

use App\Models\Language;
use App\Models\UserInterface;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class UserInterfaceController extends Controller
{
    
    public function __construct(){
        
        $this->middleware('admin');
        
    }
    
    /**
     * Display a listing of the resource.
     * 
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $country = UserInterface::all();

        $interfaces = array();

        foreach($country->all() as $key => $val){
            $interfaces[$val->id]['language'] = Language::findOrNew($val->language_id)->lang_code;
        }

        return view('admin.userInterface.index')
            ->with('interfaces', $interfaces);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $language = Language::all();

        $languages = array();

        foreach($language->all() as $key => $val){
            $languages[$val->id] = $val->language;
        }

        return view('admin.userInterface.create')
            ->with('languages', $languages);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();

        $this->validate($request, [
            'language_id' => 'required',
        ]);


        $interface = new UserInterface();
        $interface->language_id = $input['language_id'];
        unset($input['language_id']);
        unset($input['_token']);
        $interface->json_translate = json_encode($input);
        $interface->save();

        return redirect()->route('admin.user-interface.index')->with('info', 'Interface is create!');
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
        
        $interface = UserInterface::findOrFail($id);

        $language = Language::all();

        $languages = array();

        foreach($language->all() as $key => $val){
            $languages[$val->id] = $val->language;
        }

        $new_interface = json_decode($interface->json_translate, true);
        $new_interface['id'] = $interface->id;
        $new_interface['language_id'] = $interface->language_id;
        
        $interface = json_decode(json_encode($new_interface));
        
        //dd($interface);

        return view('admin.userInterface.edit')
            ->with('languages', $languages)
            ->with('interface', $interface);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        

        $input = $request->all();

        $this->validate($request, [
            'language_id' => 'required',
        ]);

        $interface = UserInterface::findOrFail($id);
        $interface->language_id = $input['language_id'];
        unset($input['language_id']);
        unset($input['_token']);
        $interface->json_translate = json_encode($input);
        $interface->save();

        return redirect()->route('admin.user-interface.index')->with('info', 'Interface is update!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

<?php

namespace App\Http\Controllers\Admin;

use Auth;
use App\Models\Language;
use App\Models\Country;
use App\Models\Category;
use App\Models\Links;
use Illuminate\Http\Request;
use App\Models\AccessCountry;
use yajra\Datatables\Datatables;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class LinksController extends Controller
{
    
    public function __construct(){
        
        $this->middleware('auth');
        
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.links.index');
    }

    public function getdata()
    {
        if(Auth::user()->role != 'admin'){
            $arr_access = AccessCountry::select('country_id')->where('user_id', '=', Auth::user()->id)->get();

            $country_arr = array();

            foreach($arr_access->all() as $val){
                $country_arr[] = $val->country_id;
            }

            $link = Links::whereIn('id', $country_arr )->where('active', '=', 1)->get();
        } else {
            $link = Links::where('active', '=', 1)->get();
        }


        return Datatables::of($link)
            ->addColumn('category', function ($list) {

                return Category::findOrNew($list->category_id)->category;
            })
            ->addColumn('country', function ($list) {
                 return Country::findOrNew($list->country_id)->country;
            })
            ->addColumn('language', function ($list) {
                 return Language::findOrNew($list->language_id)->language;
            })
            ->addColumn('activation', function ($list) {
                if($list->published){
                    return 'Active';
                } else {
                    return 'Inactive';
                }
            })
            ->addColumn('action', function ($list) {
                $button_add = '<a href="/admin/links/'.$list->id.'/edit" class="btn btn-xs btn-primary"> Edit</a>&nbsp;';
                $button_add .= '<a href="/admin/links/'.$list->id.'/delete" class="btn btn-xs btn-primary">Delete</a>&nbsp;';
                return $button_add;
            })
            ->make(true);
        //return view('voucher.voucher');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $language = Language::all();

        $languages = array();

        foreach($language->all() as $key => $val){
            $languages[$val->id] = $val->language;
        }

        $country = Country::all();

        $countrys = array();

        foreach($country->all() as $key => $val){
            $countrys[$val->id] = $val->country;
        }

        $category = Category::all();

        $categorys = array();

        foreach($category->all() as $key => $val){
            $categorys[$val->id] = $val->category;
        }

        return view('admin.links.create')
            ->with('languages', $languages)
            ->with('categorys', $categorys)
            ->with('countrys', $countrys);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();

        $this->validate($request, [
            'language_id' => 'required|numeric|min:1',
            'country_id' => 'required|numeric|min:1',
            'category_id' => 'required|numeric|min:1',
            'link' => 'required',
            'title' => 'required',
            'count_click' => 'numeric',
            'likes' => 'numeric',
            'icon'       => 'required|mimes:jpeg,bmp,png'
        ]);

        $input['user_id'] = 1;

        $imageName = rand(100000, 10000000) . '.' .
            $request->file('icon')->getClientOriginalExtension();

        $request->file('icon')->move(
            base_path() . '/public/upload/', $imageName
        );

        if(!isset($input['published'])){
            $input['published'] = 0;
        }
        $input['icon'] = $imageName;
        $input['active'] = 1;

        $input['count_click'] = 0;

        Links::create($input);

        return redirect()->route('admin.links.index')->with('info', 'Links is create!');
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $links = Links::findOrFail($id);

        $language = Language::all();

        $languages = array();

        foreach($language->all() as $key => $val){
            $languages[$val->id] = $val->language;
        }

        $country = Country::all();

        $countrys = array();

        foreach($country->all() as $key => $val){
            $countrys[$val->id] = $val->country;
        }

        $category = Category::all();

        $categorys = array();

        foreach($category->all() as $key => $val){
            $categorys[$val->id] = $val->category;
        }

        return view('admin.links.edit')
            ->with('languages', $languages)
            ->with('categorys', $categorys)
            ->with('countrys', $countrys)
            ->with('links', $links);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->all();

        $links = Links::findOrFail($id);

        $this->validate($request, [
            'language_id' => 'required|numeric|min:1',
            'country_id' => 'required|numeric|min:1',
            'category_id' => 'required|numeric|min:1',
            'link' => 'required',
            'title' => 'required',
            'count_click' => 'numeric',
            'likes' => 'numeric',
        ]);

        $input['user_id'] = 1;

        if(!isset($input['published'])){
            $input['published'] = 0;
        }
        if(isset($input['icon'])){
            $imageName = rand(100000, 10000000) . '.' . $request->file('icon')->getClientOriginalExtension();

            $request->file('icon')->move(
                base_path() . '/public/upload/', $imageName
            );

            $input['icon'] = $imageName;
        }

        $links->fill($input)->save();

        return redirect()->route('admin.links.index')->with('info', 'Links is update!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $links = Links::findOrFail($id);

        $links->delete();

        return redirect()->route('admin.links.index')->with('info', 'Links is delete!');
    }
}

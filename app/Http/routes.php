<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function() {
    $lang = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);
    Session::put('lang', $lang);
    return redirect('/'.  $lang);
});

Route::get('/{lang}', [
    'uses'=>'HomeController@index',
]);

Route::post('/{lang}', [
    'uses'=>'HomeController@index',
    'as'=>'home',
]);

Route::get('/click/{id}', [
    'uses'=>'HomeController@click',
    'as'=>'click',
]);

Route::post('/ajax/like', [
    'uses'=>'HomeController@like',
    'as'=>'like',
]);

Route::post('/ajax/add-link', [
    'uses'=>'HomeController@AddLink',
    'as'=>'add-link',
]);

Route::post('/ajax/get-new-captcha', [
    'uses'=>'HomeController@newCaptcha',
    'as'=>'get-new-captcha',
]);
Route::get('/link/activation/{hash}', [
    'uses'=>'HomeController@activation_link',
    'as'=>'activation_link',
]);

/**
 * Authentication
 */

Route::get('/auth/signin', [
    'uses'=>'Auth\AuthController@getSignin',
    'as'=>'auth.signin',
]);

Route::get('/auth/signout', [
    'uses'=>'Auth\AuthController@getSignout',
    'as'=>'auth.signout',
]);

Route::post('/auth/signin', [
    'uses'=>'Auth\AuthController@postSignin',
    'as'=>'auth.signin',
]);



Route::group(['middleware' => 'auth'], function () {
    Route::group(['prefix' => 'admin'], function () {
        resource('language', 'admin\LanguageController');
        resource('country', 'admin\CountryController');
        resource('category', 'admin\CategoryController');
        resource('category-translate', 'admin\CategoryTranslateController');
        resource('links', 'admin\LinksController');
        resource('user', 'admin\UserController');
        resource('user-interface', 'admin\UserInterfaceController');
        resource('accessCountry', 'admin\AccessCountryController');
        
        Route::get('/settings', [
            'uses'=>'admin\SettingsController@index',
            'as'=>'admin.settings.index',
        ]);
        Route::get('/settings/{settings}/edit', [
            'uses'=>'admin\SettingsController@edit',
            'as'=>'admin.settings.edit',
        ]);
        
        Route::PATCH('/settings/{settings}', [
            'uses'=>'admin\SettingsController@update',
            'as'=>'admin.settings.update',
        ]);

        Route::get('/ajax/getdata', [
            'uses'=>'admin\LinksController@getdata',
            'as'=>'admin.link.getdata',
        ]);

        Route::get('/links/{id}/delete', [
            'uses'=>'admin\LinksController@destroy',
            'as'=>'admin.link.delete',
        ]);



    });
});
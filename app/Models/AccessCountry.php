<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AccessCountry extends Model  {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'access_country';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id','country_id'];

}
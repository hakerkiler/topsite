<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserInterface extends Model  {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'user_interface';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['language_id','json_translate'];

}
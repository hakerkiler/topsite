<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model  {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'category';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['category'];

}
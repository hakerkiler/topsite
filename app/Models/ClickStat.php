<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ClickStat extends Model  {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'click_stat';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['link_id'];

}
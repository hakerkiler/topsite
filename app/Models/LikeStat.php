<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LikeStat extends Model  {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'like_stat';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['link_id','ip_user','like'];

}
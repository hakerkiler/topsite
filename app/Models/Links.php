<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Links extends Model  {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'links';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'category_id',
        'country_id',
        'language_id',
        'user_id',
        'title',
        'link',
        'likes',
        'count_click',
        'published',
        'icon',
        'active',
        'hash_activation'
    ];

}
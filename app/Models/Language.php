<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Language extends Model  {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'language';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['lang_code','language'];

    public static function getBrowserLang(){
        $lang = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);

        return $lang;
    }

}
<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CategoryTranslate extends Model  {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'category_translate';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['language_id','category_id','translate'];

}
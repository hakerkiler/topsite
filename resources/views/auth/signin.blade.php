@extends('template.default')

@section('content')

    <h3>Sign In </h3>
    <div class="row">
        <div class="col-lg-4">
            {!! Form::open(['route' => 'auth.signin']) !!}

            <div class="form-group">
                {!! Form::label('email', 'Email', ['class' => 'control-label']) !!}
                {!! Form::text('email', null, ['class' => 'form-control']) !!}
            </div>

            <div class="form-group">
                {!! Form::label('password', 'Password', ['class' => 'control-label']) !!}
                {!! Form::password('password', ['class' => 'form-control']) !!}
            </div>

            <div class="form-group">
                {!! Form::label('remember', 'Remember', ['class' => 'control-label']) !!}
                {!! Form::checkbox('remember', 1, ['class' => 'form-control']) !!}
            </div>
            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
            {!! Form::submit('Sign In', ['class' => 'btn btn-primary']) !!}

            {!! Form::close() !!}

        </div>
    </div>


@stop
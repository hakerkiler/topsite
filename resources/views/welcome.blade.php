@extends('template.default')

@section('content')

    <div class="row">
        <div class="col-lg-12">
            @foreach($lang as $l)
                @if(http_build_query($_GET))
                    <a href="/{{ $l->lang_code }}?{{ http_build_query($_GET) }}" class="btn btn-info">{{ $l->language }}</a>
                @else
                    <a href="/{{ $l->lang_code }}" class="btn btn-info">{{ $l->language }}</a>
                @endif
            @endforeach
        </div>
    </div>
    
    <h3>{{ (isset($interface->header_title))? $interface->header_title : 'Link in system' }}</h3>
    <div class="row">
        <div class="col-lg-12">

            {!! Form::open(['method' => 'GET','route' => ['home', Session::get('lang')], 'class' => 'form-inline']) !!}

            <div class="form-group col-lg-3">
                {!! Form::label('country', (isset($interface->country))? $interface->country : 'Country', ['class' => 'control-label']) !!}
                {!! Form::select('country', $countrys, (isset($_GET['country']))? $_GET['country'] : 0, ['class' => 'form-control']) !!}
            </div>
            <div class="form-group col-lg-3">
                <a class="btn btn-primary" role="button" data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                    {{ isset($interface->category)? $interface->category : 'Category' }}
                </a>
            </div>
            <div class="form-group col-lg-3">
                {!! Form::label('sortable', (isset($interface->sortable))? $interface->sortable : 'Sortable', ['class' => 'control-label']) !!}
                {!! Form::select('sortable', ['likes' => (isset($interface->like))? $interface->like : 'Like' , 'count_click' => (isset($interface->count_click))? $interface->count_click : 'Count Click'], (isset($_GET['sortable']))? $_GET['sortable'] : 'likes', ['class' => 'form-control']) !!}
            </div>
            <div class="form-group col-lg-2">
            {!! Form::submit((isset($interface->show))? $interface->show : 'Show', ['class' => 'btn btn-primary ']) !!}
            </div>

            <div class="clearfix"></div>
            <br>
            <div class="collapse col-lg-12" id="collapseExample">
              <div class="well" style="display: inline-block;width: 100%;">
                @foreach($categorys as $key => $val)
                    @if(isset($_GET['category']) && in_array($key, $_GET['category']))

                        <div class="checkbox col-lg-3" > <label> <input type="checkbox" name="category[]" checked value="{{ $key }}"> {{ $val }} </label> </div>
                    @else
                        <div class="checkbox col-lg-3" > <label> <input type="checkbox" name="category[]" value="{{ $key }}"> {{ $val }} </label> </div>
                    @endif
                @endforeach
              </div>
            </div>

            {!! Form::close() !!}
<br>

            <table class="table table-striped projects link_table">
                @if( count($links) > 0 )
                    <tbody>
                    @foreach( $links as $key => $link )
                        @if( count($link['link_list']) > 0 )
                        <tr>
                            <td>
                                {{ $link['category'] }}
                            </td>
                            @foreach( $link['link_list'] as $list )
                            <td class="link_icon">
                                <div class="text-center">
                                    <a target="_blank" href="{{ route('click', $list->id) }}">
                                        <img height="30" src="{{ '/upload/' .$list->icon }}" alt="{{ $list->title }}" title="{{ $list->title }}">
                                    </a>
                                </div>
                                <div class="like_div">
                                    <a class="btn btn-default btn-sm glyphicon glyphicon-thumbs-up like_click_up" data-link_id="{{ $list->id }}"></a>
                                    <a class="btn btn-default btn-sm glyphicon glyphicon-thumbs-down like_click_down" data-link_id="{{ $list->id }}"></a>
                                </div>
                            </td>
                            @endforeach
                            @for ($x = 0; $x < 10 - count($link['link_list']); $x++)
                                <td></td>
                            @endfor
                        </tr>
                        @endif
                    @endforeach
                    </tbody>
                @endif
            </table>
            @if($count > 10)
            <ul class="pagination">
                @for ($i = 0; $i < $count/10 ; $i++)
                    @if(isset($_GET) && count($_GET) > 0 && !array_key_exists('page', $_GET))
                    <li><a href="?page={{ $i+1 }}&{{ http_build_query($_GET) }}">{{ $i+1 }}</a></li>
                    @elseif(isset($_GET) && array_key_exists('page', $_GET))
                    <?php unset($_GET['page']); ?>
                    <li><a href="?page={{ $i+1 }}&{{ http_build_query($_GET) }}">{{ $i+1 }}</a></li>
                    @else
                    <li><a href="?page={{ $i+1 }}">{{ $i+1 }}</a></li>
                    @endif
                @endfor
            </ul>
            @endif
        </div>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="myModal" role="dialog" style="z-index: 9999999">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">{{ (isset($interface->add_link)) ? $interface->add_link : 'Add Link' }}</h4>
                </div>
                <div class="modal-body">
                    {!! Form::open(['method' => 'post','route' => ['home', Session::get('lang')]]) !!}

                    <div class="form-group">
                        {!! Form::label('country', (isset($interface->country))? $interface->country : 'Country', ['class' => 'control-label']) !!}
                        {!! Form::select('country', $countrys, null, ['class' => 'form-control']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('category', (isset($interface->category))? $interface->category : 'Category', ['class' => 'control-label']) !!}
                        {!! Form::select('category', $categorys, null, ['class' => 'form-control']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('link', (isset($interface->link))? $interface->link : 'Link', ['class' => 'control-label']) !!}
                        {!! Form::text('link', null, ['class' => 'form-control']) !!}
                    </div>
                    @if($settings['email_comfirm'])
                    <div class="form-group">
                        {!! Form::label('email', (isset($interface->email))? $interface->email : 'Email', ['class' => 'control-label']) !!}
                        {!! Form::text('email', null, ['class' => 'form-control']) !!}
                    </div>
                    @endif
                    <div class="form-group">
                        <p class="new_captcha"> {!! captcha_img() !!}</p>
                        <p><input type="text" name="captcha"></p>
                    </div>
                    <div class="form-group">
                        <input class="btn btn-primary add_new_link" type="button" value="{{ (isset($interface->send))? $interface->send : 'Send' }}">
                    </div>

                    {!! Form::close() !!}
                </div>
            </div>

        </div>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="myModalInfo" role="dialog" style="z-index: 9999999">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content text-center">
                <div class="modal-body">

                </div>
            </div>

        </div>
    </div>


@stop
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Top Site</title>

        <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

        <link rel="stylesheet" href="/css/app.css">

    </head>
    <body>
        <!-- Go to www.addthis.com/dashboard to customize your tools -->
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5665cb7b0b8df419" async="async"></script>
        {{--@include('templates.partials.navigation')--}}
        <nav class="navbar navbar-default" role="navigation">
            <div class="container">
                <div class="navbar-header">
                    <a class="navbar-brand" href="/">GRUPIM</a>
                </div>
                <div class="collapse navbar-collapse">
                    @if(Auth::check())
                    <ul class="nav navbar-nav">
                        @if(Auth::user()->role == 'admin')
                        <li><a href="{{ route('admin.language.index') }}">Language</a></li>
                        <li><a href="{{ route('admin.country.index') }}">Country</a></li>
                        <li><a href="{{ route('admin.category.index') }}">Category</a></li>
                        <li><a href="{{ route('admin.category-translate.index') }}">Category Translates</a></li>
                        @endif
                        <li><a href="{{ route('admin.links.index') }}">Links</a></li>
                        @if(Auth::user()->role == 'admin')
                        <li><a href="{{ route('admin.user.index') }}">User</a></li>
                        <li><a href="{{ route('admin.user-interface.index') }}">User Interface</a></li>
                        <li><a href="{{ route('admin.settings.index') }}">Settings</a></li>
                        <li><a href="{{ route('admin.accessCountry.index') }}">AccessCountry</a></li>
                        @endif
                    </ul>
                    @endif
                    <ul class="nav navbar-nav navbar-right">
                        @if(!Auth::check())
                            <li><a href="#" id="add_link">Add Link</a></li>
                            <li><a href="{{ route('auth.signin') }}">Sign In</a></li>

                        @else

                            <li><a href="{{ route('auth.signout') }}">Sign Out</a></li>
                        @endif
                    </ul>
                </div>
            </div>
        </nav>


        @if($errors->any())
            <div class="alert alert-danger">
                @foreach($errors->all() as $error)
                    <p>{{ $error }}</p>
                @endforeach
            </div>
        @endif
        @if(Session::has('info'))
            <div class="alert alert-success">
                {{ Session::get('info') }}
            </div>
        @endif
        <div class="container">
            @yield('content')
        </div>


    <script>
        $(document).ready(function(){
            var csrf = '{{ csrf_token() }}';

            $('.like_click_up').click(function(){
                var link_id = $(this).attr('data-link_id');

                $.post('{{ route('like') }}', {like:1, link_id:link_id, _token:csrf }).done(function(data){
                    $('#myModalInfo').modal();
                    $('#myModalInfo .modal-body').html(data.message);
                });
            });

            $('.like_click_down').click(function(){
                var link_id = $(this).attr('data-link_id');

                $.post('{{ route('like') }}', {like:0, link_id:link_id, _token:csrf }).done(function(data){
                    $('#myModalInfo').modal();
                    $('#myModalInfo .modal-body').html(data.message);
                });
            });
            
            $('.new_captcha').click(function(){
                $('.new_captcha').html('Loading ...');
                $.post('{{ route('get-new-captcha') }}',{ _token:csrf }).done(function(data){
                    $('.new_captcha').attr('src', data.link);
                    $('.new_captcha').html('<p><img src="' + data.link + '"></p>');
                    console.log(data.link);
                });
            });

            $('#add_link').click(function(){
                $('#myModal').modal();
                return false;
            });

            $('.add_new_link').click(function(){
                var country = $('#myModal .modal-body select[name="country"] option:selected').val();
                var category = $('#myModal .modal-body select[name="category"] option:selected').val();
                var link = $('#myModal .modal-body input[name="link"]').val();
                var captcha = $('#myModal .modal-body input[name="captcha"]').val();

                $.post('{{ route('add-link') }}', {country:country, category:category, link:link, captcha:captcha, _token:csrf })
                        .done(function(data){
                            if(data.status == 1){
                                alert(data.message);
                                $('#myModal .close').trigger('click');
                                $('#myModal form')[0].reset();
                            } else {
                                if(data.message.captcha){
                                    alert(data.message.captcha);
                                }
                            }
                        });
                return false;
            });
        });
    </script>
    </body>
</html>
@extends('template.default')

@section('content')

    <h3>Links create form!</h3>
    <div class="row">
        <div class="col-lg-6">
            <a href="{{ route('admin.links.index') }}" class="btn btn-info">List Links</a>
            {!! Form::model($links, [
                'method' => 'PATCH',
                'files' => true,
                'route' => ['admin.links.update', $links->id]
            ]) !!}

            @include('admin.links.form')

            {!! Form::submit('Update link', ['class' => 'btn btn-primary']) !!}

            {!! Form::close() !!}
        </div>
    </div>


@stop



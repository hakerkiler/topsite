@extends('template.default')

@section('content')
    <style>
        #table_list_link tr td{
            width: 20px!important;
        }
        #table_list_link tr th{
            width: 20px!important;
        }
        #table_list_link tr th input{
            width: 100px!important;
        }
    </style>
    <h3>Links list in sistem!</h3>
    <div class="row">
        <div class="col-lg-12">
            <a href="{{ route('admin.links.create') }}" class="btn btn-info">New Links</a>
            <br>
            <br>
            <table id="table_list_link" cellspacing="0" width="100%">
                <thead>
                <tr class="search_input">
                    <th>ID</th>
                    <th>Category</th>
                    <th>Country</th>
                    <th>Language</th>
                    <th>Title</th>
                    <th>Likes</th>
                    <th>Clicks</th>
                    <th>Active</th>
                </tr>
                <tr class="header_field">
                    <th>ID</th>
                    <th>Category</th>
                    <th>Country</th>
                    <th>Language</th>
                    <th>Title</th>
                    <th>Likes</th>
                    <th>Clicks</th>
                    <th>Active</th>
                    <th>#Edit</th>
                </tr>
                </thead>
                <tbody>
                </tbody>
            </table>

        </div>
    </div>
    <link href="//cdn.datatables.net/1.10.9/css/jquery.dataTables.min.css" rel="stylesheet">
    <script src="//cdn.datatables.net/1.10.9/js/jquery.dataTables.min.js"></script>
    <script>
        $(document).ready(function(){
            $('#table_list_link').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{{ route('admin.link.getdata') }}',
                columns: [
                    {data: 'id', name: 'id'},
                    {data: 'category', name: 'category'},
                    {data: 'country', name: 'Country'},
                    {data: 'language', name: 'language'},
                    {data: 'title', name: 'title'},
                    {data: 'likes', name: 'likes'},
                    {data: 'count_click', name: 'clicks'},
                    {data: 'activation', name: 'activation'},
                    {data: 'action', name: 'action', orderable: false, searchable: false}
                ],
                initComplete: function () {
                    var table_all = this.api();
                    this.api().columns().every(function () {
                        var column = this;
                        //console.log(column.header());
                        var input = document.createElement("input");
                        //console.log($('#table_list_voucher .search_input th'));
                        $(input).appendTo($('.search_input th').empty())
                                .on('change', function () {
                                    //console.log($(this).parent().index());
                                    table_all.column($(this).parent().index()).search($(this).val()).draw();
                                });
                    });
                }
            });
        });
    </script>
@stop
<div class="form-group">
    {!! Form::label('category_id', 'Category:', ['class' => 'control-label']) !!}
    {!! Form::select('category_id', $categorys, null, ['class' => 'form-control']) !!}
</div>
<div class="form-group">
    {!! Form::label('country_id', 'Country:', ['class' => 'control-label']) !!}
    {!! Form::select('country_id', $countrys, null, ['class' => 'form-control']) !!}
</div>
<div class="form-group">
    {!! Form::label('language_id', 'Language:', ['class' => 'control-label']) !!}
    {!! Form::select('language_id', $languages, null, ['class' => 'form-control']) !!}
</div>
<div class="form-group">
    {!! Form::label('title', 'Title', ['class' => 'control-label']) !!}
    {!! Form::text('title', null, ['class' => 'form-control']) !!}
</div>
<div class="form-group">
    {!! Form::label('link', 'Link', ['class' => 'control-label']) !!}
    {!! Form::text('link', null, ['class' => 'form-control']) !!}
</div>
@if(isset($links))
<div class="form-group">
    {!! Form::label('likes', 'Likes', ['class' => 'control-label']) !!}
    {!! Form::text('likes', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('count_click', 'Count Click', ['class' => 'control-label']) !!}
    {!! Form::text('count_click', null, ['class' => 'form-control']) !!}
</div>
@endif
<div class="form-group">
    {!! Form::label('published', 'Published', ['class' => 'control-label']) !!}
    {!! Form::checkbox('published', 1, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('icon', 'Icon', ['class' => 'control-label']) !!}
    {!! Form::file('icon', null, ['class' => 'form-control']) !!}
</div>
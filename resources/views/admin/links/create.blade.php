@extends('template.default')

@section('content')

    <h3>Links create form!</h3>
    <div class="row">
        <div class="col-lg-6">
            <a href="{{ route('admin.links.index') }}" class="btn btn-info">List Links</a>
            {!! Form::open(['route' => 'admin.links.store', 'files' => true]) !!}

            @include('admin.links.form')

            {!! Form::submit('Create links', ['class' => 'btn btn-primary']) !!}

            {!! Form::close() !!}
        </div>
    </div>


@stop



@extends('template.default')

@section('content')

    <h3>Category create form!</h3>
    <div class="row">
        <div class="col-lg-6">
            <a href="{{ route('admin.category.index') }}" class="btn btn-info">List Category</a>
            {!! Form::open(['route' => 'admin.category.store']) !!}

            @include('admin.category.form')

            {!! Form::submit('Create Category', ['class' => 'btn btn-primary']) !!}

            {!! Form::close() !!}
        </div>
    </div>


@stop



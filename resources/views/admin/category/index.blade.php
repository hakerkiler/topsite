@extends('template.default')

@section('content')

    <h3>Category list in sistem!</h3>
    <div class="row">
        <div class="col-lg-6">
            <a href="{{ route('admin.category.create') }}" class="btn btn-info">New Category</a>
            <table class="table table-striped projects">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Category</th>
                    <th style="width: 20%">#Edit</th>
                </tr>
                </thead>
                @if( count($categorys) > 0 )
                    <tbody>
                    @foreach( $categorys as $category )
                        <tr>
                            <td>#</td>
                            <td>
                                {{ $category->category }}
                            </td>
                            <td>
                                <a href="{{ route('admin.category.edit', $category->id) }}" class="btn btn-primary btn-xs"><i class="fa fa-folder"></i> Edit </a>
                                {!! Form::open([
                                        'method' => 'DELETE',
                                        'route' => ['admin.category.destroy', $category->id]
                                    ]) !!}
                                {!! Form::submit('Delete', ['class' => 'btn btn-danger btn-xs']) !!}
                                {!! Form::close() !!}

                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                @endif
            </table>

        </div>
    </div>


@stop
@extends('template.default')

@section('content')

    <h3>Category create form!</h3>
    <div class="row">
        <div class="col-lg-6">
            <a href="{{ route('admin.category.index') }}" class="btn btn-info">List Category</a>
            {!! Form::model($category, [
                'method' => 'PATCH',
                'route' => ['admin.category.update', $category->id]
            ]) !!}

            @include('admin.category.form')

            {!! Form::submit('Update Category', ['class' => 'btn btn-primary']) !!}

            {!! Form::close() !!}
        </div>
    </div>


@stop



@extends('template.default')

@section('content')

    <h3>Category Translates create form!</h3>
    <div class="row">
        <div class="col-lg-6">
            <a href="{{ route('admin.category-translate.index') }}" class="btn btn-info">List Category Translates</a>
            {!! Form::open(['route' => 'admin.category-translate.store']) !!}

            @include('admin.categoryTranslates.form')

            {!! Form::submit('Create Translates', ['class' => 'btn btn-primary']) !!}

            {!! Form::close() !!}
        </div>
    </div>


@stop



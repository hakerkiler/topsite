@extends('template.default')

@section('content')

    <h3>Category Translates list in sistem!</h3>
    <div class="row">
        <div class="col-lg-6">
            <a href="{{ route('admin.category-translate.create') }}" class="btn btn-info">New Category Translates</a>
            <table class="table table-striped projects">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Language</th>
                    <th>Category</th>
                    <th>Translate</th>
                    <th style="width: 20%">#Edit</th>
                </tr>
                </thead>
                @if( count($categoryTranslates) > 0 )
                    <tbody>
                    @foreach( $categoryTranslates as $key => $categoryTranslate )
                        <tr>
                            <td>#</td>
                            <td>
                                {{ $categoryTranslate['language'] }}
                            </td>
                            <td>
                                {{ $categoryTranslate['category'] }}
                            </td>
                            <td>
                                {{ $categoryTranslate['translate'] }}
                            </td>
                            <td>
                                <a href="{{ route('admin.category-translate.edit', $key) }}" class="btn btn-primary btn-xs"><i class="fa fa-folder"></i> Edit </a>
                                {!! Form::open([
                                        'method' => 'DELETE',
                                        'route' => ['admin.category-translate.destroy', $key]
                                    ]) !!}
                                {!! Form::submit('Delete', ['class' => 'btn btn-danger btn-xs']) !!}
                                {!! Form::close() !!}

                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                @endif
            </table>

        </div>
    </div>


@stop
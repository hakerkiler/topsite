<div class="form-group">
    {!! Form::label('language_id', 'Language:', ['class' => 'control-label']) !!}
    {!! Form::select('language_id', $languages, null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('category_id    ', 'Category:', ['class' => 'control-label']) !!}
    {!! Form::select('category_id', $categorys, null, ['class' => 'form-control']) !!}
</div>
<div class="form-group">
    {!! Form::label('translate', 'Translate:', ['class' => 'control-label']) !!}
    {!! Form::text('translate', null, ['class' => 'form-control']) !!}
</div>
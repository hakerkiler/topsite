@extends('template.default')

@section('content')

    <h3>Country list in sistem!</h3>
    <div class="row">
        <div class="col-lg-6">
            <a href="{{ route('admin.country.create') }}" class="btn btn-info">New Country</a>
            <table class="table table-striped projects">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Language</th>
                    <th>Country Name</th>
                    <th style="width: 20%">#Edit</th>
                </tr>
                </thead>
                @if( count($countries) > 0 )
                    <tbody>
                    @foreach( $countries as $key => $country )
                        <tr>
                            <td>#</td>
                            <td>
                                {{ $country['language'] }}
                            </td>
                            <td>
                                {{ $country['name'] }}
                            </td>
                            <td>
                                <a href="{{ route('admin.country.edit', $key) }}" class="btn btn-primary btn-xs"><i class="fa fa-folder"></i> Edit </a>
                                {!! Form::open([
                                        'method' => 'DELETE',
                                        'route' => ['admin.country.destroy', $key]
                                    ]) !!}
                                {!! Form::submit('Delete', ['class' => 'btn btn-danger btn-xs']) !!}
                                {!! Form::close() !!}

                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                @endif
            </table>

        </div>
    </div>


@stop
<div class="form-group">
    {!! Form::label('language_id', 'Language:', ['class' => 'control-label']) !!}
    {!! Form::select('language_id', $languages, null, ['class' => 'form-control']) !!}
</div>
<div class="form-group">
    {!! Form::label('country', 'Country Name:', ['class' => 'control-label']) !!}
    {!! Form::text('country', null, ['class' => 'form-control']) !!}
</div>
@extends('template.default')

@section('content')

    <h3>Country create form!</h3>
    <div class="row">
        <div class="col-lg-6">
            <a href="{{ route('admin.country.index') }}" class="btn btn-info">List Country</a>
            {!! Form::model($country, [
                'method' => 'PATCH',
                'route' => ['admin.country.update', $country->id]
            ]) !!}

            @include('admin.country.form')

            {!! Form::submit('Update Country', ['class' => 'btn btn-primary']) !!}

            {!! Form::close() !!}
        </div>
    </div>


@stop



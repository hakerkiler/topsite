@extends('template.default')

@section('content')

    <h3>User edit form!</h3>
    <div class="row">
        <div class="col-lg-6">
            <a href="{{ route('admin.category.index') }}" class="btn btn-info">List User</a>
            {!! Form::model($user, [
                'method' => 'PATCH',
                'route' => ['admin.user.update', $user->id]
            ]) !!}

            @include('admin.user.form')

            {!! Form::submit('Update User', ['class' => 'btn btn-primary']) !!}

            {!! Form::close() !!}
        </div>
    </div>


@stop



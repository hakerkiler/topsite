@extends('template.default')

@section('content')

    <h3>User list in sistem!</h3>
    <div class="row">
        <div class="col-lg-6">
            <a href="{{ route('admin.user.create') }}" class="btn btn-info">New User</a>
            <table class="table table-striped projects">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Email</th>
                    <th>Role</th>
                    <th style="width: 20%">#Edit</th>
                </tr>
                </thead>
                @if( count($users) > 0 )
                    <tbody>
                    @foreach( $users as $user )
                        <tr>
                            <td>#</td>
                            <td>
                                {{ $user->email }}
                            </td>
                            <td>
                                {{ $user->role }}
                            </td>
                            <td>
                                <a href="{{ route('admin.user.edit', $user->id) }}" class="btn btn-primary btn-xs"><i class="fa fa-folder"></i> Edit </a>
                                {!! Form::open([
                                        'method' => 'DELETE',
                                        'route' => ['admin.user.destroy', $user->id]
                                    ]) !!}
                                {!! Form::submit('Delete', ['class' => 'btn btn-danger btn-xs']) !!}
                                {!! Form::close() !!}

                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                @endif
            </table>

        </div>
    </div>


@stop
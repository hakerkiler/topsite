@extends('template.default')

@section('content')

    <h3>User create form!</h3>
    <div class="row">
        <div class="col-lg-6">
            <a href="{{ route('admin.user.index') }}" class="btn btn-info">List User</a>
            {!! Form::open(['route' => 'admin.user.store']) !!}

            @include('admin.user.form')

            {!! Form::submit('Create User', ['class' => 'btn btn-primary']) !!}

            {!! Form::close() !!}
        </div>
    </div>


@stop



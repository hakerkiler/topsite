
<div class="form-group">
    {!! Form::label('email', 'Email', ['class' => 'control-label']) !!}
    {!! Form::text('email', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('password', 'Password', ['class' => 'control-label']) !!}
    {!! Form::password('password', ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('country_id', 'Country:', ['class' => 'control-label']) !!}
    {!! Form::select('country_id', $countrys, null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('language_id', 'Language:', ['class' => 'control-label']) !!}
    {!! Form::select('language_id', $languages, null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('full_name', 'Full Name', ['class' => 'control-label']) !!}
    {!! Form::text('full_name', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('role', 'Role:', ['class' => 'control-label']) !!}
    {!! Form::select('role', ['moderator'=>'Moderator','admin'=>'Admin'], null, ['class' => 'form-control']) !!}
</div>
@extends('template.default')

@section('content')

    <h3>AccessCountry list in sistem!</h3>
    <div class="row">
        <div class="col-lg-6">
            <a href="{{ route('admin.accessCountry.create') }}" class="btn btn-info">New AccessCountry</a>
            <table class="table table-striped projects">
                <thead>
                <tr>
                    <th>#</th>
                    <th>User</th>
                    <th>Country Name</th>
                    <th style="width: 20%">#Edit</th>
                </tr>
                </thead>
                @if( count($AccessCountrys) > 0 )
                    <tbody>
                    @foreach( $AccessCountrys as $key => $AccessCountry )
                        <tr>
                            <td>#</td>
                            <td>
                                {{ $AccessCountry['user'] }}
                            </td>
                            <td>
                                {{ $AccessCountry['country'] }}
                            </td>
                            <td>
                                <a href="{{ route('admin.accessCountry.edit', $key) }}" class="btn btn-primary btn-xs"><i class="fa fa-folder"></i> Edit </a>
                                {!! Form::open([
                                        'method' => 'DELETE',
                                        'route' => ['admin.accessCountry.destroy', $key]
                                    ]) !!}
                                {!! Form::submit('Delete', ['class' => 'btn btn-danger btn-xs']) !!}
                                {!! Form::close() !!}

                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                @endif
            </table>

        </div>
    </div>


@stop
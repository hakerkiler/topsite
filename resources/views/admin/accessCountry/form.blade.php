<div class="form-group">
    {!! Form::label('user_id', 'User:', ['class' => 'control-label']) !!}
    {!! Form::select('user_id', $users, null, ['class' => 'form-control']) !!}
</div>
<div class="form-group">
    {!! Form::label('country_id', 'Country:', ['class' => 'control-label']) !!}
    {!! Form::select('country_id', $countrys, null, ['class' => 'form-control']) !!}
</div>
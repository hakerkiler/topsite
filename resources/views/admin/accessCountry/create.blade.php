@extends('template.default')

@section('content')

    <h3>AccessCountry create form!</h3>
    <div class="row">
        <div class="col-lg-6">
            <a href="{{ route('admin.accessCountry.index') }}" class="btn btn-info">List AccessCountry</a>
            {!! Form::open(['route' => 'admin.accessCountry.store']) !!}

            @include('admin.accessCountry.form')

            {!! Form::submit('Create AccessCountry', ['class' => 'btn btn-primary']) !!}

            {!! Form::close() !!}
        </div>
    </div>


@stop




<div class="form-group">
    {!! Form::label('language_id', 'Language:', ['class' => 'control-label']) !!}
    {!! Form::select('language_id', $languages, null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('header_title', 'Header Title:', ['class' => 'control-label']) !!}
    {!! Form::text('header_title', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('country', 'Country:', ['class' => 'control-label']) !!}
    {!! Form::text('country', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('category', 'Category:', ['class' => 'control-label']) !!}
    {!! Form::text('category', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('select_category', 'Select Category:', ['class' => 'control-label']) !!}
    {!! Form::text('select_category', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('sortable', 'Sortable', ['class' => 'control-label']) !!}
    {!! Form::text('sortable', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('add_link', 'Add Link:', ['class' => 'control-label']) !!}
    {!! Form::text('add_link', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('sign_in', 'Sign In:', ['class' => 'control-label']) !!}
    {!! Form::text('sign_in', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('link', 'Link:', ['class' => 'control-label']) !!}
    {!! Form::text('link', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('email', 'Email:', ['class' => 'control-label']) !!}
    {!! Form::text('email', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('show', 'Show:', ['class' => 'control-label']) !!}
    {!! Form::text('show', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('send', 'Send:', ['class' => 'control-label']) !!}
    {!! Form::text('send', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('like', 'Like:', ['class' => 'control-label']) !!}
    {!! Form::text('like', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('count_click', 'Count Click:', ['class' => 'control-label']) !!}
    {!! Form::text('count_click', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('like_is_write', 'Mesage if client already like', ['class' => 'control-label']) !!}
    {!! Form::text('like_is_write', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('like_write', 'Mesage new like', ['class' => 'control-label']) !!}
    {!! Form::text('like_write', null, ['class' => 'form-control']) !!}
</div>


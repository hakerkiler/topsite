@extends('template.default')

@section('content')

    <h3>Interface list in sistem!</h3>
    <div class="row">
        <div class="col-lg-6">
            <a href="{{ route('admin.user-interface.create') }}" class="btn btn-info">New Interface</a>
            <table class="table table-striped projects">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Language</th>
                    <th style="width: 20%">#Edit</th>
                </tr>
                </thead>
                @if( count($interfaces) > 0 )
                    <tbody>
                    @foreach( $interfaces as $key => $interface )
                        <tr>
                            <td>#</td>
                            <td>
                                {{ $interface['language'] }}
                            </td>
                            <td>
                                <a href="{{ route('admin.user-interface.edit', $key) }}" class="btn btn-primary btn-xs"><i class="fa fa-folder"></i> Edit </a>
                                {!! Form::open([
                                        'method' => 'DELETE',
                                        'route' => ['admin.user-interface.destroy', $key]
                                    ]) !!}
                                {!! Form::submit('Delete', ['class' => 'btn btn-danger btn-xs']) !!}
                                {!! Form::close() !!}

                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                @endif
            </table>

        </div>
    </div>


@stop
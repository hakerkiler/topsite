@extends('template.default')

@section('content')

    <h3>Interfac create form!</h3>
    <div class="row">
        <div class="col-lg-6">
            <a href="{{ route('admin.user-interface.index') }}" class="btn btn-info">List interfac</a>
            {!! Form::open(['route' => 'admin.user-interface.store']) !!}

            @include('admin.userInterface.form')

            {!! Form::submit('Create interfac', ['class' => 'btn btn-primary']) !!}

            {!! Form::close() !!}
        </div>
    </div>


@stop



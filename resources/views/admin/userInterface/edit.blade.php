@extends('template.default')

@section('content')

    <h3>Interface update form!</h3>
    <div class="row">
        <div class="col-lg-6">
            <a href="{{ route('admin.user-interface.index') }}" class="btn btn-info">List interface</a>
            {!! Form::model($interface, [
                'method' => 'PATCH',
                'route' => ['admin.user-interface.update', $interface->id]
            ]) !!}

            @include('admin.userInterface.form')

            {!! Form::submit('Update interface', ['class' => 'btn btn-primary']) !!}

            {!! Form::close() !!}
        </div>
    </div>


@stop



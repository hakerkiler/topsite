@extends('template.default')

@section('content')

    <h3>Language create form!</h3>
    <div class="row">
        <div class="col-lg-6">
            <a href="{{ route('admin.language.index') }}" class="btn btn-info">List Language</a>
            {!! Form::open(['route' => 'admin.language.store']) !!}

            @include('admin.language.form')

            {!! Form::submit('Create Language', ['class' => 'btn btn-primary']) !!}

            {!! Form::close() !!}
        </div>
    </div>


@stop



<div class="form-group">
    {!! Form::label('lang_code', 'Code Language:', ['class' => 'control-label']) !!}
    {!! Form::text('lang_code', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('language', 'Language:', ['class' => 'control-label']) !!}
    {!! Form::text('language', null, ['class' => 'form-control']) !!}
</div>
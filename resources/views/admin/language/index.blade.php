@extends('template.default')

@section('content')

    <h3>Language list in sistem!</h3>
    <div class="row">
        <div class="col-lg-6">
            <a href="{{ route('admin.language.create') }}" class="btn btn-info">New Language</a>
            <table class="table table-striped projects">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Lang Code</th>
                    <th>Language Name</th>
                    <th style="width: 20%">#Edit</th>
                </tr>
                </thead>
                @if( count($languages) > 0 )
                    <tbody>
                    @foreach( $languages as $language )
                        <tr>
                            <td>#</td>
                            <td>
                                {{ $language->lang_code }}
                            </td>
                            <td>
                                {{ $language->language }}
                            </td>
                            <td>
                                <a href="{{ route('admin.language.edit', $language->id) }}" class="btn btn-primary btn-xs"><i class="fa fa-folder"></i> Edit </a>
                                {!! Form::open([
                                        'method' => 'DELETE',
                                        'route' => ['admin.language.destroy', $language->id]
                                    ]) !!}
                                {!! Form::submit('Delete', ['class' => 'btn btn-danger btn-xs']) !!}
                                {!! Form::close() !!}

                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                @endif
            </table>

        </div>
    </div>


@stop
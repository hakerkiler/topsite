@extends('template.default')

@section('content')

    <h3>Settings edit form!</h3>
    <div class="row">
        <div class="col-lg-6">
            <a href="{{ route('admin.settings.index') }}" class="btn btn-info">List Settings</a>
            {!! Form::model($settings, [
                'method' => 'PATCH',
                'route' => ['admin.settings.update', $settings->id]
            ]) !!}

            @include('admin.settings.form')

            {!! Form::submit('Update Settings', ['class' => 'btn btn-primary']) !!}

            {!! Form::close() !!}
        </div>
    </div>


@stop



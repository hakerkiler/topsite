
<div class="form-group">
    {!! Form::label('name', 'Name:', ['class' => 'control-label']) !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('values', 'Values:', ['class' => 'control-label']) !!}
    {!! Form::select('values', [0 => 'Inactive', 1 => 'Active'], null, ['class' => 'form-control']) !!}
</div>
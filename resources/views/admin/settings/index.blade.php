@extends('template.default')

@section('content')

    <h3>Settings list in sistem!</h3>
    <div class="row">
        <div class="col-lg-6">
            <table class="table table-striped projects">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th>Value</th>
                    <th style="width: 20%">#Edit</th>
                </tr>
                </thead>
                @if( count($settings) > 0 )
                    <tbody>
                    @foreach( $settings as $setting )
                        <tr>
                            <td>#</td>
                            <td>
                                {{ $setting->name }}
                            </td>
                            <td>
                                {{ $setting->values }}
                            </td>
                            <td>
                                <a href="{{ route('admin.settings.edit', $setting->id) }}" class="btn btn-primary btn-xs"><i class="fa fa-folder"></i> Edit </a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                @endif
            </table>

        </div>
    </div>


@stop
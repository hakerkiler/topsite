<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class LikeStatistic extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		 Schema::create('like_stat',function(Blueprint $table){
		    $table->increments('id');
		    $table->integer('link_id');
		    $table->string('ip_user');
		    $table->string('like');
		    $table->timestamps();
		  });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('like_stat');
	}

}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Users extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users',function(Blueprint $table){
	            $table->increments('id');
	            $table->string('email');
	            $table->string('password');
	            $table->integer('country_id');
	            $table->integer('language_id');
	            $table->string('role');
	            $table->string('remember_token');
	            $table->string('full_name')->nullable();
	            $table->timestamps();
	        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}

}

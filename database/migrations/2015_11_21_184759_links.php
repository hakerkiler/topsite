<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Links extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
	
        Schema::create('links',function(Blueprint $table){
            $table->increments('id');
            $table->integer('category_id');
            $table->integer('country_id');
			$table->string('language_id',10);
            $table->integer('user_id');
            $table->string('title');    //add title
            $table->string('link');
            $table->integer('likes');
            $table->integer('count_click');
            $table->boolean("published")->default(false);
            $table->boolean("activation_code")->default('');
            $table->boolean("email")->default('');
            $table->string('icon');
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('links');
	}

}

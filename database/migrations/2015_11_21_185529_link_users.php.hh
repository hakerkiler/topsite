<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class LinkUsers extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		  Schema::create('links',function(Blueprint $table){
            $table->increments(('id'));
            $table->integer('category_id');
            $table->integer('country_id');
            $table->integer('user_id');
            $table->string('title');    //add title
            $table->string('lang_code',10);
            $table->string('link');
            $table->integer('likes');
            $table->integer('views');
            $table->integer('count_click');
            $table->double('rating');
            $table->boolean("published")->default(false);
            $table->string('icon');
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('links');
	}

}
